import torch
from torch import nn


class ccm_layer(nn.Module):
    def __init__(self):
        super(ccm_layer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)

        self.conv0 = nn.Conv1d(1, 1, kernel_size=9, padding=4, bias=False)
        self.conv1 = nn.Conv1d(1, 1, kernel_size=17, padding=8, bias=False)
        self.conv2 = nn.Conv1d(1, 1, kernel_size=33, padding=16, bias=False)
        self.conv3 = nn.Conv1d(1, 1, kernel_size=65, padding=32, bias=False)
        self.sigmoid = nn.Sigmoid()
        self.line = nn.Linear(4, 1, bias=False)

    def forward(self, x):

        y = self.avg_pool(x).squeeze(-1).transpose(1, 2)
        y0 = self.conv0(y).transpose(1, 2).unsqueeze(-1)
        y1 = self.conv1(y).transpose(1, 2).unsqueeze(-1)
        y3 = self.conv2(y).transpose(1, 2).unsqueeze(-1)
        y4 = self.conv3(y).transpose(1, 2).unsqueeze(-1)
        y_full = self.line(torch.cat([y0, y1, y3, y4], 2).squeeze(-1).squeeze(-1)).unsqueeze(-1)
        y = self.sigmoid(y_full)

        return x * y.expand_as(x)