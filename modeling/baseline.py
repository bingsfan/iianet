from __future__ import absolute_import
from __future__ import division

import torch
from torch import nn
from torch.nn import functional as F
import torchvision
from .backbones.resnet import ResNet
from modeling.IANet.IA import IABlock2D
from .GFF import conv_sigmoid, conv_bn_relu, DenseBlock
from .Multi_scale import MulScaleBlock
from .ccm_block import ccm_layer

def Upsample(x, size):
    """
    Wrapper Around the Upsample Call
    """
    return nn.functional.interpolate(x, size=size, mode='bilinear', align_corners=True)


class Baseline(nn.Module):
    in_planes = 2048

    def __init__(self, num_classes):
        super(Baseline, self).__init__()

        resnet50 = ResNet(last_stride= 1)

        self.conv1 = resnet50.conv1
        self.bn1 = resnet50.bn1
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = resnet50.maxpool

        self.layer1 = self._inflate_reslayer(resnet50.layer1)
        self.layer2 = self._inflate_reslayer(resnet50.layer2, IA_idx=[3], height=32,
                                             width=16, alpha_x=10, alpha_y=20, IA_channels=512)
        self.layer3_0 = self._make_layer(MulScaleBlock, 512, 1024, 6, stride=1)
        self.layer3 = self._inflate_reslayer(resnet50.layer3, IA_idx=[5], height=16,
                                             width=8, alpha_x=5, alpha_y=10, IA_channels=1024)
        self.layer4 = self._make_layer(MulScaleBlock, 1024, 2048, 3, stride=1)

        norm_layer = nn.BatchNorm2d

        self.d_in1 = conv_bn_relu(64, 128, 1, norm_layer=norm_layer)
        self.d_in2 = conv_bn_relu(2048, 128, 1, norm_layer=norm_layer)
        self.d_in3 = conv_bn_relu(512, 128, 1, norm_layer=norm_layer)

        self.gate1 = conv_sigmoid(64, 128)
        self.gate2 = conv_sigmoid(2048, 128)
        self.gate3 = conv_sigmoid(512, 128)

        in_channel = 128
        self.dense_3 = DenseBlock(in_channel, 128, 128, 3, drop_out=0, norm_layer=norm_layer)
        self.dense_6 = DenseBlock(in_channel + 128, 128, 128, 6, drop_out=0, norm_layer=norm_layer)
        self.dense_9 = DenseBlock(in_channel + 128 * 2, 128, 128, 9, drop_out=0, norm_layer=norm_layer)

        self.ccmlayer = ccm_layer()

        self.cls = nn.Conv2d(512, 2048, kernel_size=3, padding=1, bias=False)

        self.bn = nn.BatchNorm1d(2048)
        self.classifier = nn.Linear(2048, num_classes)

    def _inflate_reslayer(self, reslayer, height=0, width=0,
                          alpha_x=0, alpha_y=0, IA_idx=[], IA_channels=0):
        reslayers = []
        for i, layer2d in enumerate(reslayer):
            reslayers.append(layer2d)

            if i in IA_idx:
                IA_block = IABlock2D(in_channels=IA_channels, height=height,
                                     width=width, alpha_x=alpha_x, alpha_y=alpha_y)
                reslayers.append(IA_block)

        return nn.Sequential(*reslayers)

    def _make_layer(self, block, inplanes, planes, blocks, stride=1):
        norm_layer = nn.BatchNorm2d
        downsample = None
        if stride != 1 or inplanes != planes:
            downsample = nn.Sequential(nn.Conv2d(inplanes, planes, kernel_size=1, stride=stride, bias=False), norm_layer(planes))
        layers = []
        layers.append(block(inplanes, planes, stride, downsample))
        inplanes = planes
        for _ in range(1, blocks):
            layers.append(block(inplanes, planes))
        return nn.Sequential(*layers)

    def forward(self, x):

        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        m2 = x

        x = self.layer1(x)
        x = self.layer2(x)

        aspp = x

        x = self.layer3(x)
        x = self.layer4(x)

        m5 = x

        m2_size = m2.size()[2:]
        m5_size = m5.size()[2:]
        aspp_size = aspp.size()[2:]

        g_m2 = self.gate1(m2)
        g_m5 = self.gate2(m5)
        g_aspp = self.gate3(aspp)

        m2 = self.d_in1(m2)
        m5 = self.d_in2(m5)
        aspp = self.d_in3(aspp)
        # GFF fusion
        m2 = m2 + g_m2 * m2 + (1 - g_m2) * (Upsample(g_m5 * m5, size=m2_size) + Upsample(g_aspp * aspp, size=m2_size))
        m5 = m5 + g_m5 * m5 + (1 - g_m5) * (Upsample(g_m2 * m2, size=m5_size) + Upsample(g_aspp * aspp, size=m5_size))
        aspp_f = aspp + aspp * g_aspp + (1 - g_aspp) * (
                Upsample(g_m5 * m5, size=aspp_size) + Upsample(g_m2 * m2, size=aspp_size))
        aspp_f = Upsample(aspp_f, size=m2_size)
        aspp = Upsample(aspp, size=m2_size)
        m5 = Upsample(m5, size=m2_size)
        # DFP fusion
        out = aspp_f
        aspp_f = self.dense_3(out)
        out = torch.cat([aspp_f, m5], dim=1)
        m5 = self.dense_6(out)
        out = torch.cat([aspp_f, m5, m2], dim=1)
        m2 = self.dense_9(out)

        f = torch.cat([aspp_f, aspp, m5, m2], dim=1)

        f = self.ccmlayer(f)
        f = self.ccmlayer(f)
        x = self.cls(f)

        f = F.avg_pool2d(x, x.size()[2:])
        f = f.view(f.size(0), -1)
        f = self.bn(f)
        if not self.training:
            return f
        y = self.classifier(f)

        return y, f






